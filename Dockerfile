FROM node:12-buster-slim

# https://oracle.github.io/node-oracledb/INSTALL.html#docker
RUN apt-get update && apt-get install -y libaio1 wget unzip
RUN mkdir -p /opt/oracle && cd /opt/oracle
RUN wget https://download.oracle.com/otn_software/linux/instantclient/instantclient-basiclite-linuxx64.zip && \
    unzip instantclient-basiclite-linuxx64.zip && rm -f instantclient-basiclite-linuxx64.zip
RUN cd instantclient_21_1
RUN rm -f *jdbc* *occi* *mysql* *mql1* *ipc1* *jar uidrvci genezi adrci
RUN echo /opt/oracle/instantclient_21_1 > /etc/ld.so.conf.d/oracle-instantclient.conf && \
    ldconfig